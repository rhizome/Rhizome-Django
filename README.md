# Rhizome-Django

Projet de refonte du SI Rhizome avec le framework Django.

## Fonctionnalités
 * Remplacement de Ghost
 * Gestion des Abonnés
 * Gestion des contrats
 * Gestion du matériel
 * Gestion des inventaires
 * Gestion des Events
 * Visualisation de l'état du réseau (graphe), et des services
 * Visualisation des zones couvertes par l'association

## Environnement
Lire le fichier SETUP.md

## Contribution
Lire le fichier CONTRIBUTING.md

## Documentation
 * Les commandes minimales utiles avec Django dans CLI.md
 * Documentation de l'appli dans le dossier */doc*.
 * Documentation du framework : https://docs.djangoproject.com/en/2.1/
