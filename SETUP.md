# Setup de l'environnement de dev
Le projet étant en python 3, on utilisera un **virtualenv**.

## Créer un virtual env
Dans un terminal, se placer dans le dossier dans lequel on veut placer notre environnement virtuel, puis taper la commande:
```
python -m venv ./env_rhizome-django
```
Puis, pour activer l'environnement :
 * Si on est sous Windows
 ```
"./env_rhizome-django/Scripts/activate.bat"
 ```
 * Si on est sous Unix
 ```
source ./env_rhizome-django/bin/activate
 ```

Si votre dossier d'environnement est dans le dossier du projet, **IL NE SE COMMIT PAS** (le fichier .gitignore est là pour empêcher ça)

## Préparer l'environnement virtuel

Mettre à jour pip:
```
python -m pip install --upgrade pip
```
Installer les paquets utilisés par le projet:
```
pip install -r requirements.txt
```

Si de nouveaux paquets ont été installé pendant la session, faire, enregistrer la nouvelle liste de paquets avec la commande et commmit le fichier.
```
pip freeze > requirements.txt
```

Il est fortement conseillé de faire une mise à jour du virtualenv après chaque fetch.

Normalement l'environnement est prêt.
